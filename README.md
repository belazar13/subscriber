У меня в базе не было плагина для функции uuid_generate_v4() в базе. Быстро не нашлось, поэтому чуть поменял структуру базы

## Prepare DB
> Изменил тип поля posts.id на автоинкремент, в остальном ничего не менял
```sql
CREATE TABLE IF NOT EXISTS posts
(
    id           SERIAL PRIMARY KEY,
    lat          DOUBLE PRECISION,
    lng          DOUBLE PRECISION,
    region_label TEXT,
    created_at   TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE OR REPLACE FUNCTION notify_event() RETURNS TRIGGER AS
$$
DECLARE
    data         JSON;
    notification JSON;

BEGIN

    IF (TG_OP = 'DELETE') THEN
        data = row_to_json(OLD);
    ELSE
        data = row_to_json(NEW);
    END IF;

    notification = json_build_object(
            'table', TG_TABLE_NAME,
            'action', TG_OP,
            'data', data
        );

    PERFORM pg_notify('events', notification::TEXT);

    RETURN NULL;
END;
$$ LANGUAGE PLPGSQL;

CREATE TRIGGER posts_notify_event
    AFTER INSERT OR UPDATE OR DELETE
    ON posts
    FOR EACH ROW
EXECUTE PROCEDURE notify_event();
```

## Build APP
```text
make build
```

## Run APP
> поправить connectionString

```text
./bin/subscriber -con "postgres://user_name:user_pass@localhost:5432/postgres?sslmode=disable&connect_timeout=5"
```

## Todo
* тесты
* graceful shutdown

## Know issue
В приложении используется пул и нотиф (одинаковый) приходит сразу на все конекшены. Запросов в базу на апдейт (а так же запросов на получение из внешнего апи) будет улетать гораздо больше чем требуется - По числу конекшенов в пуле
