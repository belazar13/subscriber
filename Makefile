PROJECT_DIR?=$(shell pwd)
PROJECT_BIN_DIR=$(PROJECT_DIR)/bin
APP?=subscriber

build: clean
	CGO_ENABLED=0 go build \
	-o ./bin/${APP} ./cmd/${APP}

clean:
	rm -f bin/${APP}
