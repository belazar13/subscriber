package subscriber

import (
	"context"
	"fmt"
	"log"
	"strconv"

	"github.com/pkg/errors"

	"bitbucket.org/belazar13/subscriber/internal/provider/db"
	"bitbucket.org/belazar13/subscriber/internal/provider/geocode"
)

const (
	actionInsert = "INSERT"
)

type App struct {
	dbProvider db.Provider
	geo        geocode.Provider
}

func New(db db.Provider, geo geocode.Provider) *App {
	if db == nil {
		log.Fatal("db provider is nil")
	}

	return &App{
		dbProvider: db,
		geo:        geo,
	}
}

func (a *App) Run(chanelList ...string) error {
	ctx := context.Background()
	eventBus := make(db.NotificationChan)

	for _, c := range chanelList {
		err := a.dbProvider.WaitForNotification(ctx, eventBus, c)
		if err != nil {
			log.Fatal(err)
		}
	}

	for {
		notification, ok := <-eventBus
		if !ok {
			break
		}

		if notification.Error != nil {
			fmt.Println(errors.Wrap(notification.Error, "error from db event bus:"))
		}

		if notification.Payload != nil && notification.Action == actionInsert {
			go func() {
				name, err := a.geo.Get(&geocode.Point{
					Lat: notification.Payload.Lat,
					Lng: notification.Payload.Lng,
				})

				if err != nil {
					fmt.Println(errors.Wrap(err, "error while retrieving region label:"))
				}

				if len(name) > 0 {
					if err := a.dbProvider.UpdateRegionLabel(ctx, notification.Payload.ID, name); err != nil {
						fmt.Println(errors.Wrap(err, "error while update region label:"))
					} else {
						fmt.Println(notification.Payload.ID, name)
						//time.Sleep(1 * time.Second)
					}
				} else {
					fmt.Println("region name is empty for id:" + strconv.FormatInt(notification.Payload.ID, 10))
				}
			}()
		}
		// manage payload == nil
	}

	return nil
}
