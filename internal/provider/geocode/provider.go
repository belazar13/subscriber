package geocode

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
)

var (
	ErrPointIsNil         = errors.New("point is empty")
	ErrFeaturesAreEmpty   = errors.New("features are empty")
	ErrPropertiesAreEmpty = errors.New("properties are empty")
)

type property struct {
	Label string
}

type feature struct {
	Properties *property
}

type geoResponse struct {
	Features []*feature
}

type Point struct {
	Lat float64
	Lng float64
}

type Provider interface {
	Get(point *Point) (string, error)
}

type provider struct {
}

func (p *provider) Get(point *Point) (string, error) {
	if point == nil {
		return "", ErrPointIsNil
	}

	url := fmt.Sprintf("https://api.geocode.earth/v1/reverse?api_key=ge-5673e2c135b93a30&layers=region&point.lat=%f&point.lon=%f", point.Lat, point.Lng)
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	jsonRaw, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	geoResp := &geoResponse{}
	if err := json.Unmarshal(jsonRaw, geoResp); err != nil {
		return "", err
	}

	if len(geoResp.Features) == 0 || geoResp.Features[0] == nil {
		return "", ErrFeaturesAreEmpty
	}

	feature := geoResp.Features[0]
	if feature.Properties == nil {
		return "", ErrPropertiesAreEmpty
	}

	return feature.Properties.Label, nil
}

func New() (Provider, error) {
	return &provider{}, nil
}
