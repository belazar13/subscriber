package db

type NotificationChan chan *Notification

type Post struct {
	ID          int64
	Lat         float64
	Lng         float64
	RegionLabel string
}

type Notification struct {
	Error error

	PID     uint32
	Channel string
	Table   string
	Action  string
	Payload *Post `json:"data"`
}
