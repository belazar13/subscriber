package db

import (
	"context"
	"encoding/json"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Provider interface {
	WaitForNotification(ctx context.Context, chanel NotificationChan, dbChanelName string) error
	UpdateRegionLabel(ctx context.Context, id int64, name string) error
}

type provider struct {
	ctx  context.Context
	pool *pgxpool.Pool
}

func New(ctx context.Context, connectionString string) (Provider, error) {
	connConfig, err := pgxpool.ParseConfig(connectionString)
	if err != nil {
		return nil, err
	}

	connConfig.MinConns = 1
	connConfig.MaxConns = 2

	conn, err := pgxpool.ConnectConfig(ctx, connConfig)
	if err != nil {
		return nil, err
	}

	p := &provider{
		ctx: ctx, pool: conn,
	}

	return p, err
}

func (p *provider) WaitForNotification(ctx context.Context, chanel NotificationChan, dbChanelName string) error {
	conn, err := p.pool.Acquire(ctx)
	if err != nil {
		return err
	}

	_, err = conn.Exec(context.Background(), "listen "+dbChanelName)
	if err != nil {
		return err
	}

	go func(chanel NotificationChan) {
		for {
			notification := &Notification{}
			if err != nil {
				notification.Error = err
				chanel <- notification
				continue
			}

			dbNotification, err := conn.Conn().WaitForNotification(ctx)
			if err != nil {
				notification.Error = err
				chanel <- notification
				continue
			}

			err = json.Unmarshal([]byte(dbNotification.Payload), notification)
			if err != nil {
				notification.Error = err
				chanel <- notification
				continue
			}

			notification.PID = dbNotification.PID
			notification.Channel = dbNotification.Channel
			chanel <- notification
		}

	}(chanel)

	return nil
}

func (p *provider) UpdateRegionLabel(ctx context.Context, id int64, name string) error {
	_, err := p.pool.Exec(ctx, "UPDATE posts SET region_label=$1 WHERE id=$2", name, id)
	return err
}
