package main

import (
	"context"
	"flag"
	"log"

	"github.com/pkg/errors"

	"bitbucket.org/belazar13/subscriber/internal/provider/db"
	"bitbucket.org/belazar13/subscriber/internal/provider/geocode"
	"bitbucket.org/belazar13/subscriber/internal/subscriber"
)

type envConfig struct {
	connectionString string
}

func readConfig() (*envConfig, error) {
	var connectionString string

	flag.StringVar(&connectionString, "con", "", "postgres://user_name:user_pass@localhost:5432/postgres?sslmode=disable&connect_timeout=5")
	flag.Parse()

	if len(connectionString) == 0 {
		log.Fatal("connection string is empty")
	}

	return &envConfig{
		connectionString: connectionString,
	}, nil
}

func main() {
	ctx := context.Background()

	cfg, err := readConfig()
	if err != nil {
		log.Fatal(err)
	}

	dbProvider, err := db.New(ctx, cfg.connectionString)
	if err != nil {
		log.Fatal(err)
	}

	geoProvider, err := geocode.New()
	if err != nil {
		log.Fatal(err)
	}

	app := subscriber.New(dbProvider, geoProvider)
	if err := app.Run("events"); err != nil {
		log.Fatal(errors.Wrap(err, "can't start application:"))
	}
}
